const redis = require("redis")
// Varáveis de ambiente
const redisURL = process.env.REDIS_URL
// Conexção com o banco de dados
exports.conn = redis.createClient(redisURL).on("error", function (err) {
    console.log("Error " + err);
});