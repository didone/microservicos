const package = require('./package.json');
const jwt = require('jsonwebtoken')
const database = require('./database')
const auth = require('./model/auth.json')

// Varáveis de ambiente
const api_key = process.env.API_KEY
const secret = process.env.SECRET
//Conexão com o banco de dados
const db = database.conn
// Parser do cabeçalho de autenticação
function decodeUserPass(authorization) {
    const b64auth = (authorization || '').split(' ')[1] || ''
    return new Buffer(b64auth, 'base64').toString().split(':')
}
// Rota da versão
exports.version = function version(req, res) {
    res.status(200).send(package.version)
}

// Rota username
exports.username = function version(req, res) {
    user = auth.filter( user => user.username == req.params.username )[0]
    res.status(200).send({
                            "uuid":user.uuid,
                            "username":user.username
                        })
}
// Roda da geração do Token
exports.generateToken = function (req, res) {
    const [login, password] = decodeUserPass(req.headers.authorization)

    db.get(login, function (err, reply) {
        if (login != "" && reply == password) {
            const token = jwt.sign({ "user": login }, secret)
            res.status(200).send({
                "error": false,
                "message": 'Autenticação realizada com sucesso',
                "token": token
            })
        } else {
            res.set('WWW-Authenticate', 'Basic realm="401"')
            res.status(401).send({
                "error": true,
                "message": 'Autenticação necessária'
            })
        }
    });
}
// Rota da validação do Token
exports.validateToken = function (req, res) {
    try {
        let verify = (jwt.verify(req.headers.jwt, secret))
        verify.error = false
        verify.message = "Token Válido"
        res.status(200).send(verify)
    } catch (error) {
        return res.status(403).send({
            "error": true,
            "message": 'Token Inválido'
        });
    }
}
// Rota da criação de usuário
exports.newUser = function (req, res) {
    if (req.headers.secret_api_key == api_key) {
        let newUserName = req.body.user
        let newUserPwd = req.body.pass
        db.get(newUserName, function (err, reply) {
            if (reply) {
                res.status(501).send({
                    "error": true,
                    "message": 'Update não implementado'
                });
            } else {
                db.set(newUserName, newUserPwd, function (err) {
                    if (err) {
                        res.status(500).send({
                            "error": true,
                            "message": 'Erro interno'
                        });
                    } else {
                        res.status(201).send({
                            "error": false,
                            "message": 'Usuário criado',
                            "user": newUserName,
                            "pass": newUserPwd
                        });
                    }
                })
            }
        })
    } else {
        res.set('WWW-Authenticate', 'Basic realm="401"')
        res.status(401).send({
            "error": true,
            "message": 'Autenticação necessária'
        })
    }
}