const express = require('express')
const bodyParser = require('body-parser');
const app = express()
const rotas = require('./rotas')
// Varáveis de ambiente
const port = process.env.PORT || 3000
// Parsing application/json
app.use(bodyParser.json()); 
// Parsing application/xwww-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true })); 
// Rotas da aplicação
app.get( '/', rotas.version)
app.get( '/user/:username', rotas.username)
app.post('/new', rotas.newUser)
app.get( '/token', rotas.generateToken)
app.post('/token', rotas.validateToken)
// Start do servidor
app.listen(port, function () {
    console.log('App Start on:',port)
})