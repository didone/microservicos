# Microserviço `user`

Microserviço de usuários para o treinamento Compasso

## Pré requisitos

É necessário o [Node.js](https://nodejs.org/en/) e o [Docker](https://www.docker.com/docker-windows) estejam instalados e operacionais

## Setup Projeto

Primeiramente é necessário a criação de duas variáveis de ambiente:

* `REDIS_URL`: Que armazenará a URL de conexão com o banco de dados
* `SECRET`: Quer armazenará o segredo da chave de autenticação
* `API_KEY`: Chave privada das aplicações para poderem acessar determinados tipos de serviços

Em seguida faça o clone do repositório para um diretório local.

```ssh
git clone https://github.com/Didone/ms-user.git
```

> Se preferir utilize o [client do GitHub](https://desktop.github.com/) para fazer o clone

No diretório clonado execute s script abaixo para inicializar o servidor de aplicação

```bash
# Setup do npm
npm config set strict-ssl false
npm install supervisor -g
# Install dependencies
npm install
# Start application
npm start
```

## [Express](https://expressjs.com/)

Microframework web para Node.js

```bash
npm install express --save
```

## [Redis](https://en.wikipedia.org/wiki/Redis)

O [projeto redis](https://github.com/antirez/redis) é um banco de dados *Open Source* e *NoSQL* do tipo chave valor. Pode ser instalado em diversas plataformas e destaca-se pela velocidade e facilidade de uso.

Para esta demonstração utilizarei o redis dentro de um *container* [Docker](https://hub.docker.com/_/redis/)

```bash
docker run --rm -d -p 6379:6379 redis:latest
```

Exemplo da utilização do redis, via linha de comando

```redis
SET chave "valor"
GET chave
DEL chave
```

Para facilitar a visualização dos dados contidos no banco de dados utilize o [Redis Desktop Manager](https://redisdesktop.com/download)

## [Postman](https://www.getpostman.com/)

O Postman é um *client* útil para realizar/testar/debugar chamadas http. Consulte sua [documentação](https://www.getpostman.com/docs/v6/) para conhecer todos seus recursos.

O arquivo [user.postman.json](user.postman.json) possui as requests deste serviço para testes.

## [JWT](https://jwt.io/)

JSON Web Tokens, são uma maneira segura de geração de tokens para validação de informações. Serão utilziados para validação de autenticidade e troca de informações entre os serviços.

## [Heroku](http://heroku.com/)

É um PaaS (Plataforma como serviço) da SalesForce. Suporta diversar linguagens de desenvolvimento e bancos de dados, incluindo o Node.js e o Redis, nos quais o nosso desenvolvimento foi criado. Além disso possui uma versão free para aplicações pequenas como estas.